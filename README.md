In 1987, artisan and stone craftsman, Joseph Hage (Joe) emigrated from his native Lebanon to the United States. With over 20 years of experience in fabricating natural stone for custom projects, Joe utilized his talent and passion to establish roots in a new land.

Address: 216 W Market St, Greensboro, NC 27401, USA

Phone: 336-203-7701

Website: https://marblegraniteworld.com/greensboro-granite-countertops/
